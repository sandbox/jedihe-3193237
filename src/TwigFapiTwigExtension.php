<?php

namespace Drupal\twig_fapi;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Template\Attribute;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension.
 */
class TwigFapiTwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    $func_builder = function ($twig_func, $method) {
      return new TwigFunction($twig_func, [self::class, $method]);
    };

    $func_to_methods_map = [
      'render_to_attributes' => 'renderToAttributes',
    ];

    $functions = array_map($func_builder,
      array_keys($func_to_methods_map),
      array_values($func_to_methods_map));

    return $functions;
  }

  /** 
   * Builds an attributes object from an element.
   */
  public static function renderToAttributes(array $element) {
    RenderElement::setAttributes($element);
    // TODO: Drupal core seems to call preRenderAjaxform() internally, it should be ok to not call it here. Remove once this is fully confirmed.
    // $element = RenderElement::preRenderAjaxForm($element);

    $attr_values = array_filter([
      'id' => $element['#id'] ?? FALSE,
      'maxlength' => $element['#maxlength'] ?? FALSE,
      'name' => $element['#name'] ?? FALSE,
      'value' => $element['#type'] !== 'html_tag' && !empty($element['#value']) ? $element['#value'] : FALSE,
      'href' => $element['#type'] == 'link' ? $element['#url']->toString() : FALSE,
      'placeholder' => $element['#placeholder'] ?? FALSE,
    ]) + ($element['#attributes'] ?? []);

    $attached_only = ['#attached' => $element['#attached'] ?? []];
    \Drupal::service('renderer')->render($attached_only);

    return new Attribute($attr_values);
  }

}
