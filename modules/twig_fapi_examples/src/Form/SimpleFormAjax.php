<?php

namespace Drupal\twig_fapi_examples\Form;

use Drupal\Core\Form\FormStateInterface;

class SimpleFormAjax extends SimpleForm {

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Use a known id. Warning: this is not safe if the form is instanced
    // multiple times in a single DOM tree.
    $form['#id'] = $this->getFormId();

    // Enable ajax submission, replace the whole form.
    $form['submit']['#ajax'] = [
      'callback' => '::ajaxCallback',
      'wrapper' => $form['#id'],
      'event' => 'click',
      'disable-refocus' => TRUE,
    ];

    return $form;
  }

  public function ajaxCallback($form, FormStateInterface $form_state) {
    if (!empty($form_state->getErrors())) {
      // Validation failed, return the form so the user can correct the
      // mistake. FAPI takes care of showing error messages, setting 'error'
      // class, etc.
      return $form;
    }
    // No validation errors, return the themed status messages. Since the #ajax
    // wrapper is the form id, the entire form will be replaced with this.
    return ['#type' => 'status_messages'];
  }

}
