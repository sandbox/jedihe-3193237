<?php

namespace Drupal\twig_fapi_examples\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simple Form example.
 */
class SimpleForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'twig_fapi_examples_simple_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#description' => $this->t('Please, enter a test email address.'),
      '#default_value' => 'test@example.com',
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['email-class-from-buildForm'],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sign Up!'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('email') === 'test@example.com') {
      $form_state->setErrorByName('email', $this->t('Please, use a different email address.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email');
    $this->messenger()->addStatus($this->t('You submitted @email!', ['@email' => $email]));

  }

}
